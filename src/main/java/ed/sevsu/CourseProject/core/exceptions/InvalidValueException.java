package ed.sevsu.CourseProject.core.exceptions;

public class InvalidValueException extends Exception{
    public InvalidValueException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
