package ed.sevsu.CourseProject.core.exceptions;

public class ProductNotFoundException extends Exception {
    public ProductNotFoundException(long id) {
        super(String.format("Product with Id = %d not found", id));
    }
}