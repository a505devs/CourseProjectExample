package ed.sevsu.CourseProject.core.abstractions.database;

import ed.sevsu.CourseProject.core.entities.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> getProducts();
    void addProduct(Product product);
    void changeProduct(Product product);
    Product findById(long id);
}
