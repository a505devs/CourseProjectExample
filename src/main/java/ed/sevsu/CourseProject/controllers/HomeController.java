package ed.sevsu.CourseProject.controllers;

import ed.sevsu.CourseProject.application.dtos.CreateProductDto;
import ed.sevsu.CourseProject.application.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
    private final ProductService service;

    public HomeController(ProductService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String home(Model model) {
        var products = this.service.showProducts();
        model.addAttribute("products", products);
        return "Home";
    }

    @GetMapping("/create")
    public String create() {
        return "Create";
    }

    @PostMapping("/create")
    public String create(CreateProductDto dto) {
        service.addProduct(dto);
        return "redirect:/";
    }
}
