package ed.sevsu.CourseProject.application.services;

import ed.sevsu.CourseProject.application.dtos.CreateProductDto;
import ed.sevsu.CourseProject.application.dtos.ProductListItemDto;
import ed.sevsu.CourseProject.core.abstractions.database.ProductRepository;
import ed.sevsu.CourseProject.core.entities.Product;
import org.springframework.stereotype.Service;
import ed.sevsu.CourseProject.application.dtos.UpdateProductDto;
import ed.sevsu.CourseProject.core.exceptions.ProductNotFoundException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private final ProductRepository repository;

    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public List<ProductListItemDto> showProducts() {
        var products = this.repository.getProducts();
        var productDtos = new ArrayList<ProductListItemDto>(products.size());

        for (Product product : products) {
            var dto = new ProductListItemDto();
            dto.setId(product.getId());
            dto.setName(product.getName());
            dto.setSku(product.getSku());
            dto.setQuantity(product.getQuantity());
            dto.setCost(product.getCost().toString());
            dto.setTotal(product.totalCost().toString());

            productDtos.add(dto);
        }

        return productDtos;
    }

    public void addProduct(CreateProductDto createProductDto) {
        Product product = new Product(
                createProductDto.getName(),
                createProductDto.getSku(),
                createProductDto.getQuantity(),
                new BigDecimal(createProductDto.getCost())
        );

        this.repository.addProduct(product);
    }

    public void changeProduct(UpdateProductDto updateProductDto) throws ProductNotFoundException {
        var product = this.repository.findById(updateProductDto.getId());

        if (product == null) {
            throw new ProductNotFoundException(updateProductDto.getId());
        }
        product = new Product(
                updateProductDto.getId(),
                updateProductDto.getName(),
                updateProductDto.getSku(),
                updateProductDto.getQuantity(),
                updateProductDto.getCost()
        );

        this.repository.changeProduct(product);
    }
}
