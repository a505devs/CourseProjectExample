package ed.sevsu.CourseProject.core.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class ProductTest {

    @Test
    void testTotalCost() {
        Product product = new Product("Key1", "132", 10, new BigDecimal("22.31"));
        Assertions.assertEquals("223.10", product.totalCost().toString());
    }
}
