package ed.sevsu.CourseProject.core.entities.services;

import ed.sevsu.CourseProject.application.dtos.CreateProductDto;
import ed.sevsu.CourseProject.application.dtos.ProductListItemDto;
import ed.sevsu.CourseProject.application.dtos.UpdateProductDto;
import ed.sevsu.CourseProject.application.services.ProductService;
import ed.sevsu.CourseProject.core.abstractions.database.ProductRepository;
import ed.sevsu.CourseProject.core.entities.Product;
import ed.sevsu.CourseProject.core.exceptions.ProductNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class ProductServiceTests {
    @Test
    void showProducts_shouldReturnAllProducts() {
        // Тестовые данные в нашей базе
        var products = new ArrayList<Product>();
        products.add(new Product(1, "Булочка", "1", 2, BigDecimal.valueOf(2)));
        products.add(new Product(2, "Круассан", "2", 5, BigDecimal.valueOf(5)));
        // Создаем заглушку (тестовую реализацию)
        ProductRepository repositoryMock = Mockito.mock(ProductRepository.class);
        // Если вызывается метод getProducts()
        // то возвращаем список, созданный выше
        Mockito.doReturn(products).when(repositoryMock).getProducts();
        // Наш сервис использует тестовую заглушку
        ProductService service = new ProductService(repositoryMock);

        // Вызываем метод сервиса и сохраняем результат в переменную
        List<ProductListItemDto> dtos = service.showProducts();

        // Проверяем результат
        Assertions.assertEquals(2, dtos.size()); // два элемента в коллекции
        // Проверяем что передалось правильно
        Assertions.assertEquals("Булочка", dtos.get(0).getName());
        Assertions.assertEquals("Круассан", dtos.get(1).getName());
        // Проверяем правильность total
        Assertions.assertEquals("4", dtos.get(0).getTotal());
        Assertions.assertEquals("25", dtos.get(1).getTotal());
    }

    @Test
    void addProduct_shouldAddProductToList() {
        //Создаём тестовый Product и на его основе тестовый CreateProductDto
        Product product = new Product(1, "Чай", "1342", 50, BigDecimal.valueOf(100));
        //Создаём тестовый CreateProductDto
        CreateProductDto createProductDto = new CreateProductDto();
        createProductDto.setName("Чай");
        createProductDto.setSku("1342");
        createProductDto.setQuantity(50);
        createProductDto.setCost("100");
        //Создаём тестовый List
        var products = new ArrayList<Product>();
        products.add(new Product(1, "Булочка", "1", 2, BigDecimal.valueOf(2)));
        products.add(new Product(2, "Круассан", "2", 5, BigDecimal.valueOf(5)));
        // Тестовые данные в нашей базе
        // Создаем заглушку (тестовую реализацию)
        ProductRepository repositoryMock = Mockito.mock(ProductRepository.class);
        // Если вызывается метод addProducts() с любым в параметре Product
        // то возвращаем добавляем Product в наш список
        Mockito.doAnswer(invocation -> {
            products.add(product);
            return null;
        }).when(repositoryMock).addProduct(any(Product.class));
        // Наш сервис использует тестовую заглушку
        ProductService service = new ProductService(repositoryMock);

        //Отправляем наш CreateProductDto в метод
        service.addProduct(createProductDto);
        //Начинаем проверки
        //Проверка на кол-во элементов в List
        Assertions.assertEquals(3, products.size());
        //Проверка на соответствие полей от переданного объекта к объекту в List
        Assertions.assertEquals("Чай", products.get(2).getName());
        Assertions.assertEquals("1342", products.get(2).getSku());
        Assertions.assertEquals(50, products.get(2).getQuantity());
        Assertions.assertEquals(BigDecimal.valueOf(100), products.get(2).getCost());
    }

    @Test
    void changeProduct_shouldChangeProductInList() throws ProductNotFoundException {
        //Создаём тестовый Product
        Product product = new Product(1, "Cucumber", "SKU-new", 150, BigDecimal.valueOf(60));
        //Создаём тестовый UpdateProductDto на основе Product
        UpdateProductDto updateProductDto = new UpdateProductDto();
        updateProductDto.setId(1);
        updateProductDto.setName("Cucumber");
        updateProductDto.setSku("SKU-new");
        updateProductDto.setQuantity(150);
        updateProductDto.setCost(BigDecimal.valueOf(60));
        //Создадим тестовый список из случайных элементов
        var products = new ArrayList<Product>();
        products.add(new Product(1, "Tea", "SKU-0001", 50, BigDecimal.valueOf(10000)));
        products.add(new Product(2, "Coffee", "SKU-0002", 100, BigDecimal.valueOf(150)));
        products.add(new Product(3, "Juice", "SKU-0003", 150, BigDecimal.valueOf(35)));
        //Создаем заглушку (тестовую реализацию)
        ProductRepository repositoryMock = Mockito.mock(ProductRepository.class);
        /*
        Здесь описываем что делает Mock.
        Mockito это класс со статическими методами, который будет что-то что ты скажешь, когда что-то произойдёт.
        В нашем случае:
        1. Идёт запрос в репозиторий с изменениями продукта.
        2. Наш репозиторий это Mock, следовательно, запрос идёт ему.
        3. Mock должен изменить объект в нашем тестовом листе, когда придёт запрос changeProduct.
        Значит алгоритм такой:
        Сделать {Замена элемента в списке на наш Product}, когда в repositoryMock придёт метод changeProduct();
        Как это выглядит:
        Mockito.сделать(внутри лямбда функция, которая просто заменяет продукт с индексом 0 на наш).когда в repositoryMock.запустится метод changeProducts(про any написал немного ниже)
         */
        Mockito.doAnswer(invocation -> {
            products.set(0, product);
            return null;
        }).when(repositoryMock).changeProduct(any(Product.class));
        /*
        Также мы знаем, что в changeProduct вызывается метод findById(long id), который возвращает Product по id.
        Для этого используем ещё один Mock.
        Mockito.Возвращаем из products 1 элемент (по индексу 0).Когда в repositoryMock.Запстится метод findById(any это "что-то" определенного класса из библиотеки с Mock)
         */
        Mockito.doReturn(products.get(0)).when(repositoryMock).findById(any(Long.class));
        //Наш сервис использует тестовую заглушку
        ProductService service = new ProductService(repositoryMock);

        //Начинаем проверки
        //Запускаем сам метод
        service.changeProduct(updateProductDto);
        //Проверка на соответствие полей от переданного объекта к объекту в List
        Assertions.assertEquals("Cucumber", products.get(0).getName());
        Assertions.assertEquals("SKU-new", products.get(0).getSku());
        Assertions.assertEquals(150, products.get(0).getQuantity());
        Assertions.assertEquals(BigDecimal.valueOf(60), products.get(0).getCost());
    }
}